//
//  GameOverScene.swift
//  TimingTrip
//
//  Created by Ricardo Z Charf on 8/20/15.
//  Copyright (c) 2015 TimeLords. All rights reserved.
//


import Foundation
import SpriteKit


class GameOverScene:MiniGame{
    var miniGameDelegate: MiniGameDelegateProtocol? = nil
    var scoreTitleLabel = SKLabelNode()
    var scoreValueLabel = SKLabelNode()
    
    override func initMinigame(difficulty:Difficulty, delegate: MiniGameDelegateProtocol) {
        super.initMinigame(difficulty, delegate: delegate)
    }
    
    override func didMoveToView(view: SKView) {
        Score.sharedInstance.value = 0
        
        scoreTitleLabel = SKLabelNode(fontNamed:"Familiar Pro")
        scoreTitleLabel.text = "Score";
        scoreTitleLabel.fontSize = 60;
        scoreTitleLabel.name = "scoreTitleLabel"
        scoreTitleLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y: (1.0)*CGRectGetMidY(self.frame));
        self.addChild(scoreTitleLabel)
        
        scoreValueLabel = SKLabelNode(fontNamed:"Familiar Pro")
        scoreValueLabel.text = String(Score.sharedInstance.value)
        scoreValueLabel.fontSize = 40;
        scoreValueLabel.name = "scoreValueLabel"
        scoreValueLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y: 0.6*CGRectGetMidY(self.frame));
        self.addChild(scoreValueLabel)
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            _ = self.nodeAtPoint(location)
            minigameDelegate?.minigameDidFinishWithStatus(.GameOver)
        }
    }
    
}