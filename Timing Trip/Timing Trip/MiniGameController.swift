//
//  GameViewController.swift
//  TimingTrip
//
//  Created by Ricardo Z Charf on 8/18/15.
//  Copyright (c) 2015 TimeLords. All rights reserved.
//

import UIKit
import SpriteKit
import CoreMotion

extension SKNode {
    class func unarchiveFromFile(file : String) -> SKNode? {
        if let path = NSBundle.mainBundle().pathForResource(file, ofType: "sks") {
            let sceneData = try! NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe)
            let archiver = NSKeyedUnarchiver(forReadingWithData: sceneData)
            
            archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
            let scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as! SKScene
            archiver.finishDecoding()
            return scene
        } else {
            return nil
        }
    }
}

class MiniGameController: UIViewController, MiniGameDelegateProtocol {
    
    var timer:GameTimer!
    var debugMode = false
    var screenWidthFull:CGFloat = 1536
    var screenWidth:CGFloat = 1152
    var screenHeight:CGFloat = 2048
    
    //
    var currentMiniGame = MiniGameType.None
    var score = 0
    var lives = 1
    
    var lastMinigame = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presentStartMenu()
    }
    
    // MARK: Present Start Menu
    func presentStartMenu(){
        if let scene = StartScene(size: view.bounds.size) as StartScene!{
            scene.minigameDelegate = self
            let skView = self.view as! SKView

            if debugMode { toggleDebugMode(true, skView: skView) }
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            
            skView.presentScene(scene)
        }
    }
    
    // MARK: PresentNextMinigame
    func presentNextMinigame(){
        var scene: MiniGame!
        var randomNumber = 0        
        
        
        repeat{ // Prevents minigames from immediately repeating
            randomNumber = Int(arc4random_uniform(3))
        }while(randomNumber == self.lastMinigame)
        
        // set this number to the minigame you want to test
        //randomNumber = 0
        
        switch (randomNumber) {
        case (0):
            currentMiniGame = .SailTheSea
            print("Sea Minigame presented!");
            scene = SailTheSea(difficulty: Difficulty.Hard, delegate: self)
            scene.size = view.frame.size
        case (1):
            currentMiniGame = .RunFromDino
            print("RunFromDino presented!");
            scene = RunFromDino(difficulty: Difficulty.Hard, delegate: self)
            scene.size = view.frame.size
        case (2):
            currentMiniGame = .Piramide
            print("Piramide presented!");
            scene = PiramideScene.unarchiveFromFile("PiramideScene") as! PiramideScene!
            scene.minigameDelegate = self
            scene.difficulty = Difficulty.Hard
            scene.scaleMode = .AspectFill
        default:
            print("Fatal Error! MiniGame not found");
        }
        
        self.lastMinigame = randomNumber
                
        let transitionType = SKTransition.moveInWithDirection(SKTransitionDirection.Right, duration: 0.5)
        
        let skView = self.view as! SKView
        skView.presentScene(scene, transition: transitionType)

    }
    
    // MARK: PresentNextMinigame
    func presentTransitionScene(gameover: Bool){
        
        currentMiniGame = .TransitionScene
        
        let transitionScreen = Transition.unarchiveFromFile("transition") as! Transition
        
        transitionScreen.minigameDelegate = self
        transitionScreen.score = score
        transitionScreen.gameOver = gameover
        transitionScreen.scaleMode = .AspectFill
        
        let skView = self.view as! SKView
        skView.presentScene(transitionScreen)
    
    }
    
    func toggleDebugMode(bool: Bool, skView: SKView){
        skView.showsFPS = bool
        skView.showsNodeCount = bool
        skView.showsPhysics = bool
    }
    
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return UIInterfaceOrientationMask.Portrait
        } else {
            return UIInterfaceOrientationMask.Portrait
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    //MARK: MiniGameDelegate
    //Called when a scene finishes
    func minigameDidFinishWithStatus(status:MinigameStatus){
        if (MotionManager.sharedInstance.manager.accelerometerAvailable == true){
            MotionManager.sharedInstance.manager.stopAccelerometerUpdates()
        }
        
        switch(status) {
        case .Success:
            score++
            Score.sharedInstance.value++
            print("Transition Scene Presented")
            presentTransitionScene(false)

        case .Failure:
            lives--;
            print("Minigame Failed")
            
            if (lives == 0) {
                //GameOver
                presentTransitionScene(true)
            } else {
                presentTransitionScene(false)
            }
        case .GameOver:
            print("StartMenu")
            presentStartMenu()
            //TODO: Define a constant
            lives = 1
        case .NewGame:
            print("NewGame")
            presentNextMinigame()
    
        }
    }
    
    func presentGameOverScreen(){
        currentMiniGame = .GameOver
        let scene = GameOverScene.unarchiveFromFile("GameOverScene") as! GameOverScene!
        scene.minigameDelegate = self
        scene.scaleMode = .AspectFill
        let transitionType = SKTransition.moveInWithDirection(SKTransitionDirection.Right, duration: 0.5)
        let skView = self.view as! SKView
        skView.presentScene(scene, transition: transitionType)
    }
}
