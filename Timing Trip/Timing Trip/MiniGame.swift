//
//  MiniGame.swift
//  TimingTrip
//
//  Created by Ricardo Z Charf on 8/18/15.
//  Copyright (c) 2015 TimeLords. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit
import CoreMotion

// ============Every MiniGame MUST inherit from this class
protocol MiniGameDelegateProtocol {
    
    //Use if implement pause button or config
    //func minigameDidPause();
    
    //func minigameDidResume();
    
    //func minigameDidStart();
    
    func minigameDidFinishWithStatus(status:MinigameStatus)
}

class MiniGame: SKScene, GameTimerDelegateProtocol {
    
    var minigameDelegate:MiniGameDelegateProtocol? = nil
    var difficulty: Difficulty!
    var minigameStatus = MinigameStatus.Failure
    var miniGamePaused = true
    
    //Timer
    var timer:GameTimer!
    var timerLabel:SKLabelNode!
    var timerGauge:SKShapeNode!
    var timerGaugeShadow:SKShapeNode!
    var move:SKAction!
    var screenWidthFull:CGFloat = 1536
    var screenWidth:CGFloat = 1152
    var screenHeight:CGFloat = 2048
    
    
    //MARK: Initializers
    convenience init(difficulty:Difficulty, delegate:MiniGameDelegateProtocol){
        self.init()
        self.initMinigame(difficulty, delegate: delegate)

    }
    
    /// The game will start paused.
    func initMinigame(difficulty:Difficulty, delegate:MiniGameDelegateProtocol){
        self.minigameDelegate = delegate
        self.difficulty = difficulty
        self.miniGamePaused = true
    }
    
    ///MARK: MiniGame was presented
    ///Sets the delegate and the timer gauge and unpause the minigame
    override func didMoveToView(view: SKView) {
        self.timer = GameTimer(delegate: self)
        self.setTimerGauge()
        self.miniGamePaused = false
    }
    
    //MARK: Game Controls
    
    /// Ends the timer and call the next scene with the miniGameStatus
    //This will also call gameTimerHasFinished
    func miniGameHasEnded() {
        self.timer.finish()
        self.timerGauge.removeAllActions()
    }
    
    
    // MARK: Timer functions
    func gameTimerHasFinished() {
        print("Timer has finished")
        miniGamePaused = true
        minigameDelegate?.minigameDidFinishWithStatus(minigameStatus)
    }
    
    func gameTimerHasUpdated(timeInPercentage: Float, secondsLeft: Float) {
        //FIXME: check redundance of this SKAction (its being called more than needed)
        if(self.view != nil){
            let move = SKAction.moveToX(-self.view!.frame.width, duration: NSTimeInterval(secondsLeft))
            timerGauge.runAction(move)
        }
        
    }
    
    //MARK: Timer Gauge
    ///Sets the timer gauge
    func setTimerGauge() {
        let timerGaugeHeight = CGFloat(0.05*self.frame.width)
        
        timerGaugeShadow = SKShapeNode(rect: CGRectMake(0, self.view!.frame.height-timerGaugeHeight, self.view!.frame.width, timerGaugeHeight))
        timerGaugeShadow.fillColor = UIColor.blackColor().colorWithAlphaComponent(0.2)
        timerGaugeShadow.strokeColor = UIColor.clearColor()
        timerGaugeShadow.zPosition = 300
        self.addChild(timerGaugeShadow)
        
        timerGauge = SKShapeNode(rect: CGRectMake(0, self.view!.frame.height-timerGaugeHeight, self.view!.frame.width, timerGaugeHeight))
        timerGauge.fillColor = UIColor.silverColor()
        timerGauge.strokeColor = UIColor.clearColor()
        timerGauge.zPosition = 300
        self.addChild(timerGauge)
    }

    // MARK: Helpers
    //Load the textures from an atlas and returns it
    internal func loadTexturesFromAtlasWithName(atlasName: String) -> [SKTexture] {
        let atlas = SKTextureAtlas(named: atlasName)
        var frames = [SKTexture]()
        
        for i in 1...atlas.textureNames.count {
            let fileName = "\(atlasName)\(i)"
            let texture = atlas.textureNamed(fileName)
            frames.append(texture)
        }
        return frames
    }
    
    //MARK: Dificulty
    internal func getTimeFromDifficulty(difficulty:Difficulty) -> Float {
        switch (difficulty) {
        case (Difficulty.Tutorial):
            return 10.0
        case (Difficulty.Easy):
            return 8.0
        case (Difficulty.Medium):
            return 6.0
        case (Difficulty.Hard):
            return 4.0
        case (Difficulty.VeryHard):
            return 3.0
       
        }
    }
}
