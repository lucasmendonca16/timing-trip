//
//  StartScene.swift
//  TimingTrip
//
//  Created by Lucas Mendonça on 8/19/15.
//  Copyright (c) 2015 TimeLords. All rights reserved.
//

import SpriteKit
import UIKit

class StartScene: SKScene {
    var minigameDelegate:MiniGameDelegateProtocol? = nil
    var debugModeLabel = SKLabelNode()
    
    var sound: Button?
    var shop: Button?
    var gameCenter: Button?
    
    override func didMoveToView(view: SKView) {
        self.userInteractionEnabled = false
        createContent()
        presentButtons()
    }
    
    /**
    Function that is called when the user has touched the screen.
    */
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in (touches) {
            let touchLocation = touch.locationInNode(self)
            let touchedNode = self.nodeAtPoint(touchLocation)
           
            if(touchedNode.name == "debugModeLabel"){
                if(debugModeLabel.text == "Debug Mode: OFF"){
                    debugModeLabel.text = "Debug Mode: ON"
                    MiniGameController().debugMode = true
                    MiniGameController().toggleDebugMode(true, skView: self.view!)
                }else{
                    debugModeLabel.text = "Debug Mode: OFF"
                    MiniGameController().debugMode = false
                    MiniGameController().toggleDebugMode(false, skView: self.view!)
                }
            }else if(touchedNode.name == "sound" || touchedNode.name == "shop" || touchedNode.name == "gameCenter"){
                    //Does nothing
            }else{
                minigameDelegate?.minigameDidFinishWithStatus(.NewGame)
            }
            print("\tI've touched: \(touchedNode.name)")
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    func createContent(){
        self.backgroundColor = UIColor.wetAsphaltColor()
        
        createTitle()
        createTapToStartLabel()
        createDebugLabel()
        createButtons()
    }
    
    func createTitle(){
        let titleLabel = SKLabelNode(fontNamed:"Helvetica Neue Condensed Black")
        titleLabel.text = "Timing Trip";
        titleLabel.fontSize = 60;
        titleLabel.name = "gameTitle"
        titleLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y: 1.5*CGRectGetMidY(self.frame));
        titleLabel.zPosition = 10
        
        let titleLabelShadow = SKLabelNode(fontNamed:"Helvetica Neue Condensed Black")
        titleLabelShadow.text = "Timing Trip";
        titleLabelShadow.fontSize = 60;
        titleLabelShadow.name = "gameTitleShadow"
        titleLabelShadow.position = CGPoint(x:titleLabel.position.x, y: 0.98*titleLabel.position.y);
        titleLabelShadow.fontColor = UIColor.blackColor().colorWithAlphaComponent(0.2)
        titleLabelShadow.zPosition = 0
        
        let titleNodeContainer = SKNode()
        titleNodeContainer.addChild(titleLabel)
        titleNodeContainer.addChild(titleLabelShadow)
        self.addChild(titleNodeContainer)
        
        let goDown = SKAction.moveBy(CGVectorMake(0, -titleLabel.frame.height/10), duration: 0.8)
        let goUp = SKAction.moveBy(CGVectorMake(0, titleLabel.frame.height/10), duration: 0.6)
        let bounce = SKAction.sequence([goDown, goUp])
        
        titleNodeContainer.runAction(SKAction.repeatActionForever(bounce))
    }
    
    func createDebugLabel(){
        debugModeLabel = SKLabelNode(fontNamed:"Familiar Pro")
        debugModeLabel.text = "Debug Mode: OFF";
        debugModeLabel.fontSize = 15;
        debugModeLabel.name = "debugModeLabel"
        debugModeLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y: 0.9*(self.frame.height))
        self.addChild(debugModeLabel)
    }
    
    func createTapToStartLabel(){
        let tapToStartLabel = SKLabelNode(fontNamed:"Familiar Pro")
        tapToStartLabel.text = "TAP TO START";
        tapToStartLabel.fontSize = 30;
        tapToStartLabel.name = "tapToStartLabel"
        tapToStartLabel.position = CGPoint(x: CGRectGetMidX(self.frame), y: 0.4*self.frame.height)
        self.addChild(tapToStartLabel)
        
        let disappear = SKAction.fadeAlphaTo(0.0, duration: 0.6)
        let appear = SKAction.fadeAlphaTo(1.0, duration: 0.3)
        let pulse = SKAction.sequence([disappear, appear])
        
        tapToStartLabel.runAction(SKAction.repeatActionForever(pulse))
    }
    
    func createButtons(){
        sound = Button(imageNamed: "soundOn", otherImageNamed: "soundOff")
        sound!.name = "sound"
        
        shop = Button(imageNamed: "shop")
        shop!.name = "shop"
        
        gameCenter = Button(imageNamed: "gameCenter")
        gameCenter!.name = "gameCenter"
    }
    
    func presentButtons() {
        let timeToShow = 0.3
        let buttonWidth = sound!.button.size.width
        
        let spaceBetween = (self.frame.width - 3*buttonWidth)/4.0
    
        //print("\tspaceBtwn: \(100*spaceBetween / self.frame.width)%")
        
        sound!.position = CGPointMake(spaceBetween + buttonWidth/2, 0)
        shop!.position = CGPointMake(sound!.position.x + buttonWidth + spaceBetween, 0)
        gameCenter!.position = CGPointMake((shop?.position.x)! + buttonWidth + spaceBetween, 0)
        
        let y = sound!.button.size.height
        
        let fadeIn = SKAction.fadeInWithDuration(2*timeToShow)
        let moveUp = SKAction.moveBy(CGVector(dx: 0, dy: y), duration: timeToShow)
        let showUp = SKAction.group([fadeIn,moveUp])
    
        let nodesToAnimate = [sound, shop, gameCenter]

        let containerNode = SKNode()
        containerNode.name = "HUDcontainer"
        for var i=0; i<nodesToAnimate.count; i++ {
            containerNode.addChild(nodesToAnimate[i]!)
        }
        
        self.addChild(containerNode)
        
        containerNode.runAction(showUp) { () -> Void in
            self.userInteractionEnabled = true
        }
    }
}
