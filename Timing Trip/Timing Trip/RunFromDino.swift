//
//  RunFromDino.swift
//  TimingTrip
//
//  Created by Diego Silva de Carvalho on 20/08/15.
//  Copyright (c) 2015 TimeLords. All rights reserved.
//

import SpriteKit
import UIKit

class RunFromDino: MiniGame {
    
    //Initial sprite animation frame time
    var dinoFrameTime = 0.20
    var heroFrameTime = 0.05
    
    var grassAnimationTime = 0.6
    
    
    //FIXED CODE
    //Clouds
    let cloudSize = [CGSizeMake(200, 100),CGSizeMake(120, 60),CGSizeMake(160, 80)]
    var clouds = [DinoCloud]()
    var cloudsMinX: CGFloat = 0
    var cloudSpeed: [CGFloat] = [0.5,1,1.3]
    var cloudsShouldMove: Bool = false
    
    //Dinosaur
    var dinosaurSpeed: CGFloat = 3
    var dinosaurShouldMove = false
    var dinosaurMinPosOnX: CGFloat = 0
    
    //Sprites
    var hero: SKSpriteNode?
    var dino: SKSpriteNode?
    var grass: DinoGrass?
    
    //Textures
    var heroTexture: [SKTexture]?
    var dinoTexture: [SKTexture]?
    
    var heroAnimationAction: SKAction?
    
    //MARK: GameControl
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        
        //Game starts with success
        minigameStatus = MinigameStatus.Success
        userInteractionEnabled = false
        
        //Creates grass, ground, clouds, hero, dinosaur
        createContent()
        //Present the hero and the dinosaur with the initial animations
        //showHeroFading()
        showDinosaurComing()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        //When tapped moves back
        if (!(dino?.position.x < dinosaurMinPosOnX*1.2)) {
            dino!.runAction(SKAction.moveByX(-40, y: 0, duration: 0.5))
        }
    }
    
    //MARK: UPDATE
    override func update(currentTime: CFTimeInterval) {
        //Clouds moving
        if (clouds.count > 0 && cloudsShouldMove) {
            for i in 0...clouds.count-1 {
                if clouds[i].position.x < cloudsMinX {
                    clouds[i].position = getCloudRandomPosition(i)
                } else {
                    clouds[i].position.x -= cloudSpeed[i]
                }
            }
        }
        
        //Dinossaur moving control
        if(!miniGamePaused) {
            
            //Dinosaur moving control
            if(dinosaurShouldMove) {
                dino!.position.x += dinosaurSpeed
            }
            
            //MARK: Victory condition
            //Check if the dinosaur has reached the hero
            if(dino!.position.x >= hero!.frame.maxX) {
                miniGamePaused = true
                print("The dinosaur has reached the hero")
                dino!.removeAllActions()
                //TODO: Hero animation when die
                dinosaurShouldMove = false
                minigameStatus = MinigameStatus.Failure
                self.miniGameHasEnded()
            }
        }
    }
    
    //================== Starting the Game ===============
    //MARK: Creates the Game Content
    
    //Loads the minigame content
    func createContent(){
        
        //Sets backgroundColor
        backgroundColor = UIColor(red: 52/255, green: 152/255, blue: 219/255, alpha: 1)
        
        //Creates the ground
        createGround()
        
        //Creates Dinossaur
        dinoTexture = loadTexturesFromAtlasWithName("dino")
        dino = SKSpriteNode(texture: dinoTexture![0])
        dino!.setScale(0.5)
        dino!.anchorPoint = CGPointMake(1, 0)
        dinosaurMinPosOnX = dino!.frame.width*0.25
        dino!.position = CGPointMake(-dinosaurMinPosOnX, grass!.position.y)
        addChild(dino!)

        //Creates the Hero
        heroTexture = loadTexturesFromAtlasWithName("hero")
        hero = SKSpriteNode(texture: heroTexture![0])
        hero?.setScale(0.1)
        hero!.anchorPoint = CGPointMake(0.5, 0)
        hero!.position = CGPointMake(self.frame.width/2, grass!.position.y)
        addChild(hero!)
        
        //Creates clouds
        generateClouds(3)
    }
    
    //MARK: Creates the Ground
    func createGround() {
        let grassSize = CGSizeMake(self.frame.width, 0.03*self.frame.height)
        let grassPosition = CGPointMake(0, 0.15*self.frame.height)
        
        //Load ground
        grass = DinoGrass(groundSize: grassSize, animationDuration: grassAnimationTime)
        grass!.position = grassPosition
        addChild(grass!)
        
        //brown subground
        let subGround = SKShapeNode(rectOfSize: CGSizeMake(self.view!.frame.width, self.view!.frame.height/2-grassSize.height))
        subGround.fillColor = UIColor.brownColor()
        subGround.strokeColor = UIColor.clearColor()
        subGround.position  = CGPointMake(self.view!.frame.width/2, grass!.frame.origin.y-subGround.frame.height/2)
        addChild(subGround)
    }
    
    //MARK: Clouds functions
    //Generates random clouds in the sky
    func generateClouds(numberOfClouds: Int) {
        //initial Clouds
        for i in 1...numberOfClouds {
            //FIXME: i+1
            let cloud = DinoCloud(cloudSize: cloudSize[i-1])
            cloud.position = getCloudRandomInitialPosition(i)
            cloud.zPosition = -3
            cloud.alpha = 0.6
            addChild(cloud)
            clouds.append(cloud)
        }
        //Minimum position on X
        cloudsMinX = -clouds[0].frame.width
    }
    //Random position of Y, and cloud starts from the right edge
    func getCloudRandomPosition(number: Int) -> CGPoint {
        let randomY = CGFloat(arc4random_uniform(UInt32(size.height-grass!.frame.maxY))) + grass!.frame.maxY - cloudSize[number-1].height/2
        let position = CGPointMake(self.frame.width + cloudSize[number-1].width/2, randomY)
        return position
        
    }
    //Initial position the cloud appears on the screen
    func getCloudRandomInitialPosition(number: Int) -> CGPoint {
        var pos = getCloudRandomPosition(number)
        let randomX = CGFloat(arc4random_uniform(UInt32(size.width)))
        pos.x = randomX
        return pos
    }

    //Show the instruction message and starts the animations
    //MARK: Initial animations
    func showsInitialMessage() {
        
        //Instruction Label
        let instructionLabel = SKLabelNode(fontNamed:"Familiar Pro")
        instructionLabel.text = "tap to run!"
        instructionLabel.fontSize = 36.0
        instructionLabel.position = CGPointMake(self.frame.width/2, self.frame.height*0.8)
        addChild(instructionLabel)
        instructionLabel.runAction(SKAction.waitForDuration(0.5), completion: {
            //Moves the dinosaur
            self.dinosaurShouldMove = true
            
            self.showHeroBeingSurprised()
            //removes the label
            instructionLabel.runAction(SKAction.fadeAlphaTo(0, duration: 1.0), completion: {
                instructionLabel.removeFromParent()
            })
        })
    }
    
    //Makes the hero turn, jump, run and starts the moving screen
    func showHeroBeingSurprised() {
        var jumpHeight = hero!.frame.height*0.6
        let goUp = SKAction.moveBy(CGVector(dx: 0, dy: jumpHeight), duration: 0.15)
        
        hero!.xScale = -hero!.xScale
        
        hero!.runAction(goUp, completion: {
            jumpHeight = -jumpHeight
            let goDown = SKAction.moveBy(CGVector(dx: 0, dy: jumpHeight), duration: 0.2)
            self.hero!.runAction(goDown, completion: {
                self.hero!.xScale = -self.hero!.xScale
                //Moves the hero
                self.hero!.runAction(SKAction.moveToX(self.frame.width*0.8, duration: 0.5))
                self.grass!.move()
                self.timer.fireTimerWith(self.getTimeFromDifficulty(.Easy))
                self.startHeroFrameAnimation()
            })
        })
        
    }
    
    //Starts moving the Dinossaur from the edge of screen to the right
    func showDinosaurComing() {
        dino!.runAction(SKAction.moveToX(dinosaurMinPosOnX, duration: 1), completion: {
            self.userInteractionEnabled = true
            self.miniGamePaused = false
            self.cloudsShouldMove = true
            
        })
        self.showsInitialMessage()
    }
    
    //Fading animation
    func showHeroFading() {
        hero!.alpha = 0
        hero!.runAction(SKAction.fadeAlphaTo(1, duration: 0.3))
    }
    
    //MARK: Frame animations
    //Dinossaur Animation
    func startDinoFrameAnimation() {
        let dinoAnimationAction = SKAction.repeatActionForever(SKAction.animateWithTextures(dinoTexture!, timePerFrame: dinoFrameTime, resize: false, restore: true))
        dino!.runAction(dinoAnimationAction)

    }
    //Hero Animation
    func startHeroFrameAnimation() {
        self.heroAnimationAction = SKAction.repeatActionForever(SKAction.animateWithTextures(heroTexture!, timePerFrame: heroFrameTime, resize: true, restore: true))
        hero!.runAction(heroAnimationAction!)
    }
    
    //FIXME: Not being used until the sprites are right
    
    func increaseHeroSpeedWithTime(newTime: NSTimeInterval) {
        //Hero Animation
        let heroAnimationAction = SKAction.repeatActionForever(SKAction.animateWithTextures(heroTexture!, timePerFrame: newTime, resize: true, restore: true))
        hero!.runAction(heroAnimationAction)
        grass!.increaseGrassSpeedWithTime()
        
        let test = SKAction.moveByX(-5, y: 0, duration: 0.1)
        dino!.runAction(test)
    }

    //TODO: A function to return the time based on the difficulty
    //Returns the time for the miniGame based on its dificult difficulty
    
    override func getTimeFromDifficulty(difficulty:Difficulty) -> Float {
        switch (difficulty) {
        case (Difficulty.Tutorial):
            return 10.0
        case (Difficulty.Easy):
            return 5
        case (Difficulty.Medium):
            return 6
        case (Difficulty.Hard):
            return 7
        case (Difficulty.VeryHard):
            return 7
                }

    }
}