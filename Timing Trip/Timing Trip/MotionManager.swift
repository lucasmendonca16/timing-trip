//
//  MotionManager.swift
//  TimingTrip
//
//  Created by Lucas Mendonça on 9/1/15.
//  Copyright (c) 2015 Frobenious. All rights reserved.
//

import UIKit
import CoreMotion

class MotionManager: NSObject {
    static let sharedInstance = MotionManager()
    var manager = CMMotionManager()
}
