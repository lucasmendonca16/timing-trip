//
//  Button.swift
//  Timing Trip
//
//  Created by Lucas Mendonça on 9/30/15.
//  Copyright © 2015 Lucas Mendonça. All rights reserved.
//

import SpriteKit

protocol ButtonDelegate {
    func buttonTapped(node: Button)
}

class Button: SKNode {
    
    // MARK: - Declaration
    
    var button: SKSpriteNode
    var shadow: SKSpriteNode

    var whenModified: SKTexture
    var whenNotModified: SKTexture
    
    var isPressed: Bool = false
    var imageModifies: Bool = false
    var isModified: Bool = false
    
    let buttonScale:CGFloat = 0.25
    
    init(imageNamed: String?){
        
        let s: String
        
        if let imageName = imageNamed{
            //Found the image
            s = imageName
        }else{
            s = "empty"
        }
        whenNotModified = SKTexture(imageNamed: s)
        whenModified = SKTexture(imageNamed: s)
        
        button = SKSpriteNode(texture: whenNotModified)
        button.setScale(buttonScale)
        
        shadow = SKSpriteNode(texture: button.texture)
        shadow.color = UIColor.blackColor().colorWithAlphaComponent(0.2)
        shadow.colorBlendFactor = 1.0
        shadow.zPosition = button.zPosition - 1
        shadow.setScale(button.xScale)
        shadow.position = CGPointMake(button.position.x, button.position.y - 0.1*button.size.height)
        
        super.init()
        
        self.userInteractionEnabled = true
        
        self.addChild(button)
        self.addChild(shadow)
    }
    
    convenience init(imageNamed: String?, otherImageNamed: String?){
        self.init(imageNamed: imageNamed)
        
        if let otherImage = otherImageNamed{
            whenModified = SKTexture(imageNamed: otherImage)
        }else{
            whenModified = SKTexture(imageNamed: "empty")
        }
        imageModifies = true
    }
    
    /**
    Function required because Buttons can only be created programatically
    */
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    /**
    Function that is called when user has began touching the object
    */
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if(!isPressed){
            isPressed = true
            button.position.y -= 0.1*button.size.height
            button.color = SKColor.blackColor()
            button.colorBlendFactor = 0.4
        }
    }
    
    /**
    Function that is called when the user touch was cancelled by some reason.
    Examples: a dialog box popping up, phone being blocked, app crashing, etc.
    */
    override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
        super.touchesCancelled(touches, withEvent: event)
        releaseButton()
    }
    
    /**
    Function that is called when user has voluntarily stopped touching the object
    */
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        super.touchesEnded(touches, withEvent: event)
        releaseButton()
    }
    
    /**
    Function that sets object back to original state whenever user is not touching it
    */
    func releaseButton() {
        if(isPressed){
            isPressed = false
            button.position.y += 0.1*button.size.height
            button.color = SKColor.whiteColor()
            button.colorBlendFactor = 0.0
            
            if(imageModifies){
                if(isModified){
                    button.texture = whenNotModified
                    isModified = false
                }else{
                    button.texture = whenModified
                    isModified = true
                }
            }
        }
    }
    
}
