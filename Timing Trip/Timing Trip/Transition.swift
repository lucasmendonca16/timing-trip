//
//  Transition.swift
//  Timing Trip
//
//  Created by Diego on 28/09/15.
//  Copyright © 2015 Lucas Mendonça. All rights reserved.
//

import UIKit
import SpriteKit

class Transition: MiniGame {
    
    var hero: SKNode!
    var scoreLabel: SKLabelNode!
    var gameOver = false
    var score = 0
    var shareButton: SKSpriteNode?
    var playButton: SKSpriteNode?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        hero = self.childNodeWithName("hero")
        scoreLabel = self.childNodeWithName("score") as! SKLabelNode
    }
    
    override func didMoveToView(view: SKView) {
        if(gameOver) {
            
            shareButton = SKSpriteNode(imageNamed: "shareButton")
            shareButton!.name = "share"
            playButton = SKSpriteNode(imageNamed: "playAgain")
            playButton!.name = "playAgain"
            
            playButton!.setScale(0.5)
            shareButton!.setScale(0.5)
            
            presentGameOverButtons()
        } else {
            addScore()
            self.runAction(SKAction.waitForDuration(3.0), completion: {() -> Void in
                self.minigameDelegate?.minigameDidFinishWithStatus(.NewGame)
            })
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        for touch in touches {
            let location = touch.locationInNode(self)
            let node = nodeAtPoint(location)
            
            if (node.name == "share") {
                print("share")
                share()
            }
            else if (node.name == "playAgain") {
                minigameDelegate!.minigameDidFinishWithStatus(.GameOver)
            }
        
        }
    }
    
    func addScore() {
        //Shows last score
        let lastScore = score - 1
        scoreLabel.text = String(lastScore)
        //Add score with animation
        let scale = SKAction.scaleTo(1.5, duration: 0.5)
        scoreLabel.runAction(scale, completion: {
            self.scoreLabel.text = String(lastScore+1)
            self.scoreLabel.runAction(SKAction.scaleTo(1.0, duration: 0.5))
        })
    }
    
    func presentGameOverButtons() {
        let timeToShow = 0.5
        
        let fadeIn = SKAction.fadeInWithDuration(timeToShow)
        let moveUp = SKAction.moveBy(CGVector(dx: 0, dy: 40), duration: timeToShow)
        
        let show = SKAction.group([fadeIn,moveUp])
        
        shareButton!.alpha = 0
        playButton!.alpha = 0
        shareButton!.position = CGPointMake(0.6*self.size.width, 0.14*self.size.height)
        playButton!.position = CGPointMake(0.4*self.size.width, 0.14*self.size.height)
        
        addChild(shareButton!)
        addChild(playButton!)
        
        
        shareButton!.runAction(show)
        playButton!.runAction(show)

    }
    
    func share() {
        let text = "fiz \(score) pontos"
        let object = [text]
        let rootVC = self.view?.window?.rootViewController
        let activityVC = UIActivityViewController(activityItems: object, applicationActivities: nil)
        activityVC.excludedActivityTypes = [UIActivityTypeAirDrop, UIActivityTypeAddToReadingList]
        rootVC?.presentViewController(activityVC, animated: true, completion: nil)

    }
    
    
}

