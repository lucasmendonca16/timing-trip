//
//  DinoGround.swift
//  TimingTrip
//
//  Created by Diego Silva de Carvalho on 20/08/15.
//  Copyright (c) 2015 TimeLords. All rights reserved.
//

import UIKit
import SpriteKit

class DinoGrass: SKSpriteNode {
    var duration: NSTimeInterval!
    let numberOfGrassPieces = 12
    var grassWidth: CGFloat!
    let green1 = UIColor(red: 46/255, green: 204/255, blue: 113/255, alpha: 1)
    let green2 = UIColor(red: 39/255, green: 174/255, blue: 96/255, alpha: 1)
    
    init(groundSize: CGSize, animationDuration: NSTimeInterval) {
        super.init(texture: nil, color: UIColor.brownColor(), size: groundSize)
        duration = animationDuration
        grassWidth = self.size.width/CGFloat(numberOfGrassPieces-2)
                
        for i in 0...numberOfGrassPieces {
            var grassColor: UIColor!
            if(i % 2 == 0) {
                grassColor = green1
            }
            else{
                grassColor = green2
            }
            
            let grass = SKSpriteNode(color: grassColor, size: CGSizeMake(grassWidth, self.size.height))
            grass.anchorPoint = CGPointMake(0, 0.5)
            grass.position = CGPointMake(CGFloat(i)*grass.size.width, 0)
            addChild(grass)
        }
    }
    
    func increaseGrassSpeedWithTime() {
        duration = duration*0.9
        //println("GrassDuration: \(duration)")

        let animation = SKAction.moveByX(-grassWidth*2, y: 0, duration: duration)
        let reset = SKAction.moveToX(0, duration: 0)
        let animationSequence = SKAction.sequence([animation,reset])
        let grassSpeedAction = SKAction.repeatActionForever(animationSequence)
        runAction(grassSpeedAction)
    }
    
    func move() {
        let animation = SKAction.moveByX(-grassWidth*2, y: 0, duration: duration)
        let reset = SKAction.moveToX(0, duration: 0)
        let animationSequence = SKAction.sequence([animation,reset])

        let grassSpeedAction = SKAction.repeatActionForever(animationSequence)
        runAction(grassSpeedAction)
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
