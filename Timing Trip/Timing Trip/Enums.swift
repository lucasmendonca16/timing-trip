//
//  Enums.swift
//  TimingTrip
//
//  Created by Diego Silva de Carvalho on 28/08/15.
//  Copyright (c) 2015 Frobenious. All rights reserved.
//

import Foundation

enum MiniGameType {
    case SailTheSea
    case RunFromDino
    case Piramide
    case None
    case TransitionScene
    case GameOver
}

enum MinigameStatus{
    case Success
    case Failure
    case GameOver
    case NewGame
}

enum Difficulty{
    case Tutorial
    case Easy
    case Medium
    case Hard
    case VeryHard
}