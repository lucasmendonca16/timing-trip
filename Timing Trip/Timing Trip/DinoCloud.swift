//
//  DinoCloud.swift
//  TimingTrip
//
//  Created by Diego Silva de Carvalho on 20/08/15.
//  Copyright (c) 2015 TimeLords. All rights reserved.
//

import SpriteKit
import UIKit


class DinoCloud: SKShapeNode {
    var animationDuration: NSTimeInterval!
    init(cloudSize: CGSize) {
        super.init()
        
        let path = CGPathCreateWithEllipseInRect(CGRectMake(0, 0, cloudSize.width, cloudSize.height), nil)
        self.path = path
        fillColor = UIColor.whiteColor()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
