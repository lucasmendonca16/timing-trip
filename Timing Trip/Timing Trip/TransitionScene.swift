//
//  TransitionScene.swift
//  Timing Trip
//
//  Created by Lucas Mendonça on 9/21/15.
//  Copyright (c) 2015 Lucas Mendonça. All rights reserved.
//

import SpriteKit

class TransitionScene: SKScene {
    var minigameDelegate:MiniGameDelegateProtocol? = nil
    var debugModeLabel = SKLabelNode()
    var scoreTitleLabel = SKLabelNode()
    var scoreValueLabel = SKLabelNode()
    var score = 0
    var cristal: SKSpriteNode?
    override func didMoveToView(view: SKView) {
        self.backgroundColor = UIColor.blackColor()
        createContent()
        showScoreAnimation()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        
    }
    /*
    var angle = CGFloat(0.0)
    var orbitRadius = CGFloat(30.0)
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        
        let speed = CGPointMake(1, 1)
        angle += speed.x / 2000
        let newX = orbitRadius * ( cos(angle))
        let newY = orbitRadius * ( sin(angle))
        
        if ((cristal) != nil) {
            self.cristal?.position = CGPointMake(newX, newY)
            print("\(self.cristal?.position)")
        }
    }
*/
    
    func createContent(){
        
        
        let hero = SKSpriteNode(imageNamed:"hero1")
        hero.position = CGPointMake(size.width/2, size.height/2)
        hero.setScale(0.1)
        let rotation = SKAction.rotateByAngle(CGFloat(M_PI), duration:1)
        
        hero.runAction(SKAction.repeatActionForever(rotation))
        
        self.addChild(hero)
        //FIXME: cristal n funcionando
        cristal = SKSpriteNode(imageNamed:"cristal")
        cristal!.setScale(0.4)
        cristal?.position = CGPointMake(size.width/2, size.height/4)
        self.addChild(cristal!)
        
        
        let vortex = SKSpriteNode(imageNamed:"vortexpdf")
        vortex.position = CGPointMake(size.width/2, size.height/2)
        vortex.zPosition = -1
        
        vortex.runAction(SKAction.repeatActionForever(rotation))
        
        self.addChild(vortex)
        
        
        scoreTitleLabel = SKLabelNode(fontNamed:"Familiar Pro")
        scoreTitleLabel.text = "Score";
        scoreTitleLabel.fontColor = UIColor.purpleColor()
        scoreTitleLabel.fontSize = 60;
        scoreTitleLabel.name = "scoreTitleLabel"
        scoreTitleLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y: 1.6*CGRectGetMidY(self.frame));
        self.addChild(scoreTitleLabel)
        
        scoreValueLabel = SKLabelNode(fontNamed:"Familiar Pro")
        scoreValueLabel.text = String(self.score)
        scoreValueLabel.fontColor = UIColor.purpleColor()
        scoreValueLabel.fontSize = 40;
        scoreValueLabel.name = "scoreValueLabel"
        scoreValueLabel.position = CGPoint(x:CGRectGetMidX(self.frame), y: 1.5*CGRectGetMidY(self.frame));
        self.addChild(scoreValueLabel)
    }
    
    func showScoreAnimation(){
        let scaleUp = SKAction.scaleBy(1.5/1.0, duration: 0.5)
        let scaleDown = SKAction.scaleBy(1.0/1.5, duration: 0.5)
        
        let changeScoreWithAnim = SKAction.sequence([scaleUp, scaleDown])
        
        self.scoreValueLabel.runAction(changeScoreWithAnim, completion: { () -> Void in
            //Changed timer did end to pauseTimer
            self.runAction(SKAction.waitForDuration(3.0), completion: {() -> Void in
                self.minigameDelegate?.minigameDidFinishWithStatus(.NewGame)
            })
        })
    }
}