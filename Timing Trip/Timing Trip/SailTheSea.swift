//
//  SailTheSea.swift
//  TimingTrip
//
//  Created by Lucas Mendonça on 8/20/15.
//  Copyright (c) 2015 TimeLords. All rights reserved.
//

import SpriteKit
import CoreMotion

class SailTheSea: MiniGame, SKPhysicsContactDelegate {
    
    let playerCategory: UInt32    = 0x1 << 0;
    let rockCategory: UInt32      = 0x1 << 1;
    let boundsCategory: UInt32    = 0x1 << 2;
    let beachCategory: UInt32     = 0x1 << 3;

    var caravel = SKSpriteNode()
    var alreadyRotated:Bool = false
    var rock = SKSpriteNode()
    var t = NSTimeInterval()
    var beach = SKSpriteNode()
    
    var caravelTextures: [SKTexture]?
    var rockTextures: [SKTexture]?
    var beachTextures: [SKTexture]?
    
    var initialCaravelPos = CGPoint()
    
    var impulse:CGFloat = 0.0
    var movementAngle: CGFloat = 0.0

    override func initMinigame(difficulty: Difficulty, delegate: MiniGameDelegateProtocol) {
        super.initMinigame(difficulty, delegate: delegate)
    }
    
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        self.physicsWorld.gravity = CGVector(dx: 0, dy: 0)
        self.physicsWorld.contactDelegate = self
        self.minigameStatus = MinigameStatus.Success
        createContent()
        setAccelerometer()
        timer.fireTimerWith(getTimeFromDifficulty(difficulty))
        spawnRocks()
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        /* Called when a touch begins */
        
        
    }
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    func createContent(){
        
        // Setting the scene bounds for collision
        self.physicsBody = SKPhysicsBody(edgeLoopFromRect: self.frame)
        self.physicsBody!.categoryBitMask = boundsCategory;
        self.physicsBody!.restitution = 0.0
        
        // Setting the background color
        self.backgroundColor = UIColor.sea().colorWithAlphaComponent(1.0)
        
        // Creating the scene objects
        createCaravel()
        createRock()
        createBeach()
    }
    
    override func didSimulatePhysics() {
        caravel.position.y = self.initialCaravelPos.y
    }
    
    func createCaravel(){
        let caravelFrameTime = 0.1
    
        caravelTextures = loadTexturesFromAtlasWithName("caravel")
        caravel = SKSpriteNode(texture: caravelTextures![0])
        caravel.setScale(0.25)
        caravel.anchorPoint = CGPointMake(0.55, 0.3)
        
        caravel.position = CGPointMake(self.view!.frame.width/2, 0.75*self.view!.frame.height)
        initialCaravelPos = caravel.position
        
        caravel.zPosition = 5
        
        caravel.physicsBody = SKPhysicsBody(circleOfRadius: 0.2*caravel.frame.width)
        caravel.physicsBody?.dynamic = true
        caravel.physicsBody?.restitution = 0.0
        caravel.physicsBody?.affectedByGravity = false
        
        rock.physicsBody?.categoryBitMask = playerCategory
        rock.physicsBody?.contactTestBitMask = rockCategory
        rock.physicsBody?.collisionBitMask = 0
        
        addChild(caravel)
        
        caravel.runAction(SKAction.repeatActionForever(SKAction.animateWithTextures(caravelTextures!, timePerFrame: caravelFrameTime, resize: false, restore: true)))
    }
    
    func createRock() {
        let rockFrameTime = 0.25
        
        rockTextures = loadTexturesFromAtlasWithName("rock")
        rock = SKSpriteNode(texture: rockTextures![0])
        rock.setScale(0.20)
        rock.anchorPoint = CGPointMake(0.5, 0.5)
        rock.physicsBody = SKPhysicsBody(circleOfRadius: rock.frame.height/2)
        rock.physicsBody?.dynamic = true
        //rock.physicsBody?.restitution = 0.0
        rock.physicsBody?.affectedByGravity = false
        rock.zPosition = 2
        
        rock.physicsBody?.categoryBitMask = rockCategory
        rock.physicsBody?.contactTestBitMask = playerCategory
        rock.physicsBody?.collisionBitMask = 0
        
        addChild(rock)
        
        let rockPosition = CGFloat.random(0.3*self.view!.frame.width, to: 0.7*self.view!.frame.width)
        rock.position = CGPointMake(rockPosition, -rock.frame.height)
        
        rock.runAction(SKAction.repeatActionForever(SKAction.animateWithTextures(rockTextures!, timePerFrame: rockFrameTime, resize: false, restore: true)))
    }
    
    func createBeach() {
        let beachFrameTime = 0.2
        
        beachTextures = loadTexturesFromAtlasWithName("beach")
        beach = SKSpriteNode(texture: beachTextures![0])
        beach.anchorPoint = CGPointMake(0.5, 1.0)
        beach.setScale(self.frame.width/self.beach.frame.width)
        addChild(beach)
        beach.position = CGPointMake(CGRectGetMidX(self.frame), -beach.frame.height)
        beach.zPosition = -10
        beach.physicsBody?.categoryBitMask = beachCategory

        beach.runAction(SKAction.repeatActionForever(SKAction.animateWithTextures(beachTextures!, timePerFrame: beachFrameTime, resize: false, restore: true)))
    }
    
    func setAccelerometer(){
        if (MotionManager.sharedInstance.manager.accelerometerAvailable == true){
            MotionManager.sharedInstance.manager.startAccelerometerUpdatesToQueue(NSOperationQueue(), withHandler: {
                data, error in
                
                if data!.acceleration.x < 0 {
                    self.impulse = CGFloat(data!.acceleration.x*2)
                    self.movementAngle = CGFloat(-0.05*M_PI)
                }
                    
                else if data!.acceleration.x > 0 {
                    self.impulse = CGFloat(data!.acceleration.x*2)
                    self.movementAngle = CGFloat(0.05*M_PI)
                }
                
                else if data!.acceleration.x == 0 {
                    self.impulse = 0
                    self.movementAngle = 0
                }
                
                self.caravel.physicsBody?.applyImpulse(CGVectorMake(self.impulse, 0))
                
                let vx = abs(self.caravel.physicsBody!.velocity.dx)
                
                if(vx > 350){
                    self.caravel.runAction(SKAction.rotateToAngle(self.movementAngle, duration:0.2))
                }else if(vx < 100 && (self.caravel.position.x > 0.1*self.frame.width) &&
                    (self.caravel.position.x < 0.9*self.frame.width)){
                    self.caravel.runAction(SKAction.rotateToAngle(0, duration:0.2))
                }
                
                //print("caravel Pos = \(100*self.caravel.position.x/self.frame.width)")
            })
        }
    }
    
    func spawnRocks(){
        
        // number of times the rock spawns
        let n:Float = 4.0
        
        // time each rock takes to cross the screen
        t = NSTimeInterval(getTimeFromDifficulty(difficulty)/(n+3))
        
//        print("dificuldade: \(self.difficulty)")
//        print("tempo: \(getTimeFromDifficulty(difficulty))")
//        print("t (pedra): \(t)")
        
        for _ in 0...2 {
            let posY = -2.0*self.rock.frame.height
            var posX = CGFloat.random(0.35*self.view!.frame.width, to: 0.65*self.view!.frame.width)
            
            let moveRockUp = SKAction.moveToY(self.view!.frame.height + 2*self.rock.frame.height, duration: t)
            let wait = SKAction.waitForDuration(t/Double(2*n+2))
            let moveRockUpThenWait = SKAction.sequence([moveRockUp, wait])
            
            rock.runAction(moveRockUpThenWait, completion: { () -> Void in
                
                // Resetting the rock's position and angle
                posX = CGFloat.random(0.1*self.view!.frame.width, to: 0.9*self.view!.frame.width)
                self.rock.position = CGPointMake(posX, posY)
                self.rock.zRotation = 0.0
                
                // Spawning the second rock
                self.rock.runAction(moveRockUpThenWait, completion: { () -> Void in
                    
                    // Resetting the rock's position one more time
                    self.rock.position = CGPointMake(posX, posY)
                    self.rock.zRotation = 0.0
                    
                    // Spawning the third rock
                    self.rock.runAction(moveRockUpThenWait, completion: { () -> Void in
                        
                        // Resetting the rock's position one more time
                        self.rock.position = CGPointMake(posX, posY)
                        self.rock.zRotation = 0.0
                        
                        // Spawning the fourth rock
                        self.rock.runAction(moveRockUpThenWait, completion: { () -> Void in
                            
                            // If player has survived all four rocks
                            if(self.minigameStatus == MinigameStatus.Success){
                                self.spawnBeach(n)
                            }
                        })
                    })
                    
                })
            })
        }
    }
    
    func spawnBeach(n: Float){
        if (MotionManager.sharedInstance.manager.accelerometerAvailable == true){
            MotionManager.sharedInstance.manager.stopAccelerometerUpdates()
        }
        let moveBeachUp = SKAction.moveToY(self.caravel.position.y + self.caravel.frame.height/3, duration: 2*self.t)
        
        self.beach.runAction(moveBeachUp, completion: { () -> Void in
            self.runAction(SKAction.waitForDuration(self.t/Double(2*n+2)), completion: { () -> Void in
                self.miniGameHasEnded()
            })
        })
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        minigameStatus = MinigameStatus.Failure
        
        // If we need to verify which object has collided with the player, use this code
        /*if((contact.bodyA.categoryBitMask == rockCategory) && ( contact.bodyB.categoryBitMask == playerCategory)) {
            //code here
        }*/
        rock.physicsBody = nil
        
        let collisionTime = NSTimeInterval((caravel.position.y - rock.position.y)/(self.frame.height/CGFloat(self.t)))
        
        rock.runAction(SKAction.moveToY(caravel.position.y, duration: collisionTime), completion: { () -> Void in
            self.rock.removeAllActions()
            
            let rotate = SKAction.rotateByAngle(CGFloat(M_PI/4), duration:1.0)
            let fade = SKAction.fadeAlphaTo(0.0, duration: 1.0)
            let darken = SKAction.colorizeWithColor(UIColor.blackColor().colorWithAlphaComponent(0.2), colorBlendFactor: 0.8, duration: 0.2)
            
            let group = SKAction.group([rotate, fade, darken])
            
            self.rock.runAction(SKAction.group([fade,darken]))
            self.caravel.runAction(group, completion: { () -> Void in
                self.miniGameHasEnded()
            })
        })
    }
    
    func finishMiniGameWithSuccess(){

        self.minigameStatus = MinigameStatus.Success
        //self.timer.endTimer()
        self.timerGauge.removeAllActions()
        self.runAction(SKAction.waitForDuration(1.0), completion: { () -> Void in
           self.miniGameHasEnded()
        })
    }

    override func getTimeFromDifficulty(difficulty:Difficulty) -> Float {
        switch (difficulty) {
        case (Difficulty.Tutorial):
            return 30.0
        case (Difficulty.Easy):
            return 24.0
        case (Difficulty.Medium):
            return 20.0
        case (Difficulty.Hard):
            return 18.0
        case (Difficulty.VeryHard):
            return 9.0
        
        }
    }
    
    
}
