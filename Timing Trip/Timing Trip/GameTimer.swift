//
//  SKScene.swift
//  TimingTrip
//
//  Created by Ricardo Z Charf on 8/18/15.
//  Copyright (c) 2015 TimeLords. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

protocol GameTimerDelegateProtocol{
    //Called when the timer ends
     func gameTimerHasFinished();
    
    //Called every time the timer has updated
     func gameTimerHasUpdated(timeInPercentage: Float, secondsLeft: Float);
}

class GameTimer: NSObject {
    
    private var currentTime:Float = 0
    private var totalTime:Float = 0
    private var timer:NSTimer?
    var delegate:GameTimerDelegateProtocol? = nil
    
    //It must init with a delegate
    convenience init (delegate:GameTimerDelegateProtocol){
        self.init()
        self.delegate = delegate
    }
    
    ///Starts the timer with the time specified
    ///
    /// seconds: The time for the timer to start in seconds
    internal func fireTimerWith(seconds:Float){
        self.totalTime = seconds
        self.currentTime = self.totalTime
        self.timer = NSTimer.scheduledTimerWithTimeInterval(0.1, target: self, selector: "update", userInfo: nil, repeats: true)
    }
    
    ///Should be called when the game no longer needs the timer
    internal func finish() {
        self.timer?.invalidate()
        delegate?.gameTimerHasFinished()
    }
    
    //MARK: Timer private methods
    
    ///Updates the timer
    @objc private func update(){
        self.currentTime -= 0.1
        if(self.currentTime < 0.1){
            //Invalidates the timer
            self.finish()
        }else {
            delegate?.gameTimerHasUpdated(getTimeLeftAsPercentage(), secondsLeft: self.currentTime)
        }
    }
    
    private func getTimeLeftAsPercentage()->Float{
        return self.currentTime/self.totalTime
    }
}
